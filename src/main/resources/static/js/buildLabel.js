// 构建标签

// 1.设置doSearch查询参数
// 2.生成筛选标签
// 3.引入该js
// 4.layevent 监听 shaixuan 事件编写
// 5.头左工具栏加 id - 下方增加筛选按钮
// 6.增加筛选html - 保留筛选条件 - 重载筛选条件
function buildHtml(key, value) {
    /*
     <div className="layui-colla-item">
         <h2 className="layui-colla-title" style="font-weight: bold">三级子行业&emsp;&emsp;</h2>
         <div className="layui-colla-content">
             <div id="tertiary" className="layui-btn-container">
                 <button type="button" className="layui-btn layui-btn-xs layui-btn-primary">K</button>
                 <button type="button" className="layui-btn layui-btn-xs layui-btn-primary">M</button>
                 <button type="button" className="layui-btn layui-btn-xs layui-btn-primary">L</button>
             </div>
         </div>
     </div>
     */


    /*
        <div class="layui-tab">
            <ul class="layui-tab-title" id="ul">
                <li class="layui-this">二级子行业</li>
                <li>三级子行业</li>
                <li>标签</li>
                <li>投资轮次</li>
            </ul>

            <div class="layui-tab-content">

                <div class="layui-tab-item layui-show">
                    <div class="layui-btn-container" id="secondary">
                        <button class="layui-btn layui-btn-xs layui-btn-primary" value="第一个 二级子行业" >第一个 二级子行业</button>
                        <button class="layui-btn layui-btn-xs layui-btn-primary" value="第二个 二级子行业" >第二个 二级子行业</button>
                        <button class="layui-btn layui-btn-xs layui-btn-primary" value="第三个 二级子行业" >第三个 二级子行业</button>
                    </div>
                </div>

                <div class="layui-tab-item">
                    <div class="layui-btn-container" id="tertiary">
                        <button class="layui-btn layui-btn-xs layui-btn-primary" value="CDMD" >第一个 三级子行业</button>
                        <button class="layui-btn layui-btn-xs layui-btn-primary" value="CDMD" >第二个 三级子行业</button>
                        <button class="layui-btn layui-btn-xs layui-btn-primary" value="CDMD" >第三个 三级子行业</button>
                    </div>
                </div>
            </div>
        </div>
        */
    // 构建li
    var li = document.createElement('li');
    li.innerText = value[0].eventChildText;     // <li>三级子行业</li>

    // 构建tabItem <div class="layui-tab-item">
    var tabItem = document.createElement('div');
    tabItem.className = 'layui-tab-item';

    // 构建container <div class="layui-btn-container" id="secondary">
    var container = document.createElement('div');
    container.className = 'layui-btn-container';
    container.id = key;

    // 构建标签 <button class="layui-btn layui-btn-xs layui-btn-primary" value="第一个 二级子行业" >第一个 二级子行业</button>
    for (var i = 0; i < value.length; i++) {
        var button = document.createElement('button');
        button.className = 'layui-btn layui-btn-xs layui-btn-primary';
        button.innerText = value[i].childTypeText;                              // 放入innerText
        button.setAttribute('value', value[i].childTypeValue);     // 放入value值
        container.append(button);
    }

    // 生成
    $('#ul').append(li);
    tabItem.append(container);
    $('#tab').append(tabItem);

}

// 添加查询标签
function addQuery(eventType,value) {

    var eventTypeId = '#' + eventType;
    var eventTypeQuery = '#'+eventType+'Query';


    // 事件下所有button按钮点击
    $(eventTypeId).on('click', 'button', function () {

        // 如果已经有query标签 （多选：继续追加）
        if ($(eventTypeQuery).length > 0) {

            // 判断该标签值是否存在 如果已经存在 说明这次点击是取消
            var arr = $(eventTypeQuery).val().toString().split(',');

            if(arr.indexOf($(this).val()) === -1){
                // 如果不存在
                // 1. 数组追加值
                arr.push($(this).val());
                // 2. 把数组赋给value属性
                $(eventTypeQuery).val(arr);
                // 3. 给点击的按钮添加样式
                $(this).attr('class', 'layui-btn layui-btn-xs layui-btn-normal');
            }else {
                // 如果这个标签存在 否则 删除arr元素 赋值给value 再取消样式
                // 1. 删除arr元素
                arr.splice(arr.indexOf($(this).val()),1);
                // 2. 把数组赋给value属性
                $(eventTypeQuery).val(arr);
                // 3. 取消样式
                $(this).attr('class','layui-btn layui-btn-xs layui-btn-primary');
                // 如果是query仅剩一个标签被取消 也就是arr长度为0时 直接删除query标签
                if(arr.length===0){
                    $(eventTypeQuery).remove();
                    return;
                }
            }

            // 每次做完操作 将数组赋值给html 但前面的标签说明不动 只增减后面的html
            var queryArr = $(eventTypeQuery).html().toString().split("：");
            $(eventTypeQuery).html(queryArr[0].toString()+"："+arr);

        } else {
            // 如果是第一次点击 构造选中标签
            var queryLable = document.createElement("button");
            queryLable.className = 'layui-btn layui-btn-primary layui-border-black layui-btn-xs ';
            queryLable.onclick = new Function("removeQuery('" + eventType + "')");
            queryLable.value = $(this).val();
            queryLable.innerText = value[0].eventChildText+ '：' +$(this).html();
            queryLable.id = eventTypeQuery.substr(1);

            // 将标签展示在头工具栏后方
            $('#toolbar').append(queryLable);

            // 给点击的按钮添加样式
            $(this).attr('class', 'layui-btn layui-btn-xs layui-btn-normal');

        }


    })
}

// 移除查询标签
function removeQuery(eventType) {

    var eventTypeId = '#' + eventType;
    var eventTypeQuery = '#'+eventType+'Query';

    // 删除query标签
    $(eventTypeQuery).remove();

    // 清空类别下所有样式
    $(eventTypeId).children('button').each(function () {
        $(this).attr('class','layui-btn layui-btn-xs layui-btn-primary')
    });

    // 阻止冒泡
    window.event ? window.event.cancelBubble = true : e.stopPropagation();
}