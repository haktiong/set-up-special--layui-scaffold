// 用户名验证
function usernameVerify(value){
    if(value===''){
        return '请输入用户名';
    }

    if(!new RegExp("^[a-zA-Z0-9_\u4e00-\u9fa5\\s·]+$").test(value)){
        return '用户名不能有特殊字符';
    }
    if(/(^\_)|(\__)|(\_+$)/.test(value)){
        return '用户名首尾不能出现下划线\'_\'';
    }
    if(/^\d+\d+\d$/.test(value)){
        return '用户名不能全为数字';
    }
    if (value.length < 5 || value.length>18) {
        return '账号长度需在5-18位之间';
    }

    return '';
}

