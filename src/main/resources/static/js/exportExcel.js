
/* 导出excel */
function startExport(url) {
    // 出现遮罩
    var zz = layer.msg('Excel下载中...',
        {icon: 16
            ,shade: [0.5, '#f5f5f5']
            ,scrollbar: false
            ,offset: 'auto'
            , time:100000})

    const xhr = new XMLHttpRequest();
    xhr.open('GET', url);
    xhr.send(null);

    // 设置服务端的响应类型
    xhr.responseType = "blob";
    // 监听下载
    xhr.addEventListener('progress', event => {

        // 计算出百分比
        var percent = ((event.loaded / event.total) * 100).toFixed(2);
        // 已传输大小
        console.log((event.loaded/1024/1024).toFixed(2));
        // 总大小 (EasyExcel总大小无法计算)
        console.log(event.total);

    }, false);

    xhr.onreadystatechange = event => {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {

                // 判断是否大于64000
                if(xhr.getResponseHeader("listSize") ==='toLarge'){
                    layer.close(zz);
                    layer.msg('信息数量超过了64000行,无法导出', {icon: 2});
                    return;
                }

                // 文件名称
                const fileName = xhr.getResponseHeader('Content-Disposition').split(';')[1].split('=')[1]
                // 创建一个a标签用于下载
                const downLoadLink = document.createElement('a');
                // 文件名称 解码
                downLoadLink.download = decodeURI(fileName);
                downLoadLink.href = URL.createObjectURL(xhr.response);

                // 触发下载事件，IO到磁盘
                downLoadLink.click();

                // 释放内存中的资源
                URL.revokeObjectURL(downLoadLink.href);

                // 关闭加载动画
                layer.close(zz);
            } else if (xhr.status === 404 || xhr.status === 500) {
                alert('下载异常!');
            }
        }
    }

}