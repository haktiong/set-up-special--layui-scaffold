package com.easycms.layuigenerator.sys.common;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * 页面跳转控制器类
 *
 * @author: Haktiong
 * @create: 2021/8/23 13:30
 */

@Controller
public class IndexController {


    /**
     * TODO 没有权限自动跳转403
     */
    @RequestMapping("/403")
    public String to403() {
        return "403";
    }

    /**
     * 默认跳转页
     */
    @RequestMapping("/")
    public ModelAndView defaultPath() {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("index");
        return mv;
    }

    /**
     * 登录页
     */
    @RequestMapping("login")
    public ModelAndView login() {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("login");
        return mv;
    }

    /**
     * 进入首页
     */
    @RequestMapping("user/index")
    public ModelAndView index() {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("index");
        return mv;
    }

}
