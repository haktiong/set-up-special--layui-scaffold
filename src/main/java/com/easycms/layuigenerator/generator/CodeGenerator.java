package com.easycms.layuigenerator.generator;


import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

import java.util.*;

/**
 * Created by 2021年01月18日 23:55
 *
 * @author yu
 * @version V1.0
 *
 * 1. 需先配置三个 pom 依赖
 * 2. 配置 @Mapper
 * 3. 配置 @TableName
 * 4. 不是表中的字段 @TableField(exist=false) 同时 mapper.xml 中删除resultMap对应的字段
 *
 */
public class CodeGenerator {

    private static String[] scanner(String tip) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入" + tip + "：");

        String str = sc.next();
        if (str.equals("all")) {
            return null;
        } else {
            return str.split(",");
        }
    }

    public static void main(String[] args) {
        // 代码生成器
        AutoGenerator mpg = new AutoGenerator();

        // TODO 设置包名
        String parent = "com.easycms.layuigenerator.base";
        // TODO 设置响应体 R 位置
        String rPath = "com.easycms.layuigenerator.sys.common.R";

        System.out.println("后端代码生成路径："+parent);
        System.out.println("响应体位置："+rPath+"\n");

        // 全局配置
        GlobalConfig gc = new GlobalConfig();
        String oPath = System.getProperty("user.dir");
        gc.setOutputDir(oPath + "/src/main/java");
        gc.setAuthor("Haktiong");
        gc.setOpen(false);                  // 当代码生成完之后是否打开代码所在的文件夹
        gc.setFileOverride(false);          // 是否覆盖原来生成的
        gc.setServiceName("%sService");     // 自定义Service包名
        gc.setBaseResultMap(true);          // 生成resultMap
        gc.setBaseColumnList(true);         // XML中生成基础列
        gc.setDateType(DateType.ONLY_DATE); // 指定生成日期类型
        mpg.setGlobalConfig(gc);

        // 数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setUrl("jdbc:mysql://localhost:3306/graduation_park?useUnicode=true&characterEncoding=utf8&useSSL=true&serverTimezone=UTC&useSSL=false");
        dsc.setDriverName("com.mysql.cj.jdbc.Driver");
        dsc.setUsername("root");
        dsc.setPassword("root");
        dsc.setDbType(DbType.MYSQL);
        mpg.setDataSource(dsc);

        // 包配置
        PackageConfig pc = new PackageConfig();
        pc.setController("controller");
        pc.setEntity("model");
        pc.setMapper("dao");
        pc.setService("service");
        pc.setServiceImpl("service.impl");
        pc.setXml("mapper");
        pc.setParent(parent);
        mpg.setPackageInfo(pc);

        // 策略配置
        StrategyConfig strategy = new StrategyConfig();
        strategy.setNaming(NamingStrategy.underline_to_camel);          // 设置字段和表名的是否把下划线完成驼峰命名规则
        strategy.setColumnNaming(NamingStrategy.underline_to_camel);
        strategy.setEntityLombokModel(true);                            // 是否启动lombok
        strategy.setRestControllerStyle(true);                         // rest注解：@RestController
        strategy.setEntityTableFieldAnnotationEnable(true);             // 字段自动注解：@TableField
        strategy.setLogicDeleteFieldName("is_delete");              // 逻辑删除

        // 要设置生成哪些表 不设置就是生成所有的表
        strategy.setInclude(scanner("表名，多个表英文逗号分割，生成所有表请输入 \"all\""));
        strategy.setControllerMappingHyphenStyle(true);
        mpg.setStrategy(strategy);

        /**
         自定义配置
         */
        //配置自定义属性注入
        InjectionConfig cfg = new InjectionConfig() {
            //.vm模板中，通过${cfg.abc}获取属性
            @Override
            public void initMap() {
                Map<String, Object> map = new HashMap<>();
//                map.put("abc", this.getConfig().getGlobalConfig().getAuthor() + "-mp");
                map.put("abc", "自定义属性描述");
                map.put("rPath", rPath);
                this.setMap(map);
            }
        };

        TemplateConfig tc = new TemplateConfig();
        // 后端模板
        tc.setController("/templates/generator/controller.java");
        tc.setService("/templates/generator/service.java");
        tc.setServiceImpl("/templates/generator/serviceImpl.java");
        tc.setMapper("/templates/generator/mapper.java");
        // 前端模板 自定义输出配置
        List<FileOutConfig> focList = new ArrayList<>();
        // 自定义配置会被优先输出
        focList.add(new FileOutConfig("/templates/generator/list.html.vm") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                // 自定义输出文件名 ， 如果你 Entity 设置了前后缀、此处注意 xml 的名称会跟着发生变化！！
                return System.getProperty("user.dir") + "/src/main/resources/templates/"+ tableInfo.getEntityPath()
                        + "/list.html";
            }
        });
        focList.add(new FileOutConfig("/templates/generator/add.html.vm") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                // 自定义输出文件名 ， 如果你 Entity 设置了前后缀、此处注意 xml 的名称会跟着发生变化！！
                return System.getProperty("user.dir") + "/src/main/resources/templates/"+ tableInfo.getEntityPath()
                        + "/add.html";
            }
        });
        focList.add(new FileOutConfig("/templates/generator/edit.html.vm") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                // 自定义输出文件名 ， 如果你 Entity 设置了前后缀、此处注意 xml 的名称会跟着发生变化！！
                return System.getProperty("user.dir") + "/src/main/resources/templates/"+ tableInfo.getEntityPath()
                        + "/edit.html";
            }
        });
        cfg.setFileOutConfigList(focList);

        mpg.setCfg(cfg);
        mpg.setTemplate(tc);

        mpg.execute();

    }


}
