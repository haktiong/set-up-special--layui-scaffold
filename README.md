# 毕设专用Layui脚手架

#### 介绍

一个基于MybatisPlus+Velocity+Layui+SpringBoot的快捷生成前端页面和后端CRUD的脚手架。

#### 软件架构

Java，SpringBoot


#### 安装教程

1.  将代码git到本地，打开CodeGenerator，配置数据源信息。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0328/005620_06cc9feb_4865428.png "屏幕截图.png")

2. 输入表名。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0328/005629_eb524ad5_4865428.png "屏幕截图.png")

3. 运行完毕后，项目会生成前后端代码。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0328/005635_a0f11d11_4865428.png "屏幕截图.png")

4. 在index.html中配置相关路由，格式为【/xxx/list】，例如刚才生成的教师表teacher，那么右侧为【/teacher/list】。
![输入图片说明](https://images.gitee.com/uploads/images/2022/0328/005653_25e70a19_4865428.png "屏幕截图.png")

5. application.yml中配置好数据源信息。即可启动SpringBoot项目。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0328/005703_dac3d81c_4865428.png "屏幕截图.png")

5. 输入localhost:8080，一个简易版的基于Layui的带有增删改查代码的前端页面+后端就生成好了。效果如下：

![输入图片说明](https://images.gitee.com/uploads/images/2022/0328/005707_357c0041_4865428.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0328/005711_d99fe406_4865428.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0328/005717_42e76389_4865428.png "屏幕截图.png")

后台代码：

![输入图片说明](https://images.gitee.com/uploads/images/2022/0328/005723_528e2b55_4865428.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0328/005729_2b3f2afa_4865428.png "屏幕截图.png")

#### 使用说明

- 因为有不少小伙伴来找我做管理系统相关的毕业设计，本代码的初衷是为了自身开发便捷。